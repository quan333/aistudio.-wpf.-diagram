﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace AIStudio.Wpf.DiagramDesigner
{  

    /// <summary>
    /// 连接点
    /// </summary>
    [Serializable]
    [XmlInclude(typeof(ConnectorInfoItemBase))]
    public class ConnectorInfoItemBase : SelectableItemBase
    {
        public ConnectorInfoItemBase()
        {

        }

        public ConnectorInfoItemBase(ConnectorInfoBase viewmodel) : base(viewmodel)
        {
            PhysicalConnectorWidth = viewmodel.PhysicalConnectorWidth;
            PhysicalConnectorHeight = viewmodel.PhysicalConnectorHeight;
            Orientation = viewmodel.Orientation;
        }

        [JsonProperty(PropertyName = "ConnectorWidth")]
        [XmlAttribute("ConnectorWidth")]
        public double PhysicalConnectorWidth
        {
            get; set;
        }

        [JsonProperty(PropertyName = "ConnectorHeight")]
        [XmlAttribute("ConnectorHeight")]
        public double PhysicalConnectorHeight
        {
            get; set;
        }

        [XmlAttribute]
        public ConnectorOrientation Orientation
        {
            get; set;
        }



     
    }
}
