﻿using System;
using System.Collections.Generic;
using System.Text;
using AIStudio.Wpf.DiagramDesigner.Geometrys;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface IRouter
    {
        PointBase[] Get(IDiagramViewModel _, ConnectionViewModel link);
    }
}
