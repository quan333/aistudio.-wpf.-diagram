﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.SFC.ViewModels
{
    public class Simulate_ListViewModel : SFCNode
    {
        public Simulate_ListViewModel() : base(SFCNodeKinds.Simulate_List)
        {
            ItemWidth = 170;
            ItemHeight = 260;
        }

        public Simulate_ListViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public Simulate_ListViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

    }
}
