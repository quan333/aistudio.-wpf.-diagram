﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Wpf.Mind.Models
{
    public enum MindTheme
    {
        [Description("天空蓝")]
        SkyBlue,
        [Description("紧凑蓝")]
        SkyBlueMini, 
        [Description("文艺绿")]
        LiteratureGreen,
        [Description("紧凑绿")]
        LiteratureGreenMini,
        [Description("脑残粉")]
        BrainDeadPink,
        [Description("紧凑粉")]
        BrainDeadPinkMini,
        [Description("浪漫紫")]
        RomanticPurple,
        [Description("紧凑紫")]
        RomanticPurpleMini,
        [Description("清新红")]
        FreshRed,
        [Description("紧凑红")]
        FreshRedMini,
        [Description("泥土黄")]
        EarthyYellow,
        [Description("紧凑黄")]
        EarthyYellowMini,
        [Description("冷光黄")]
        CoolLightYellow,
        [Description("紧凑黄")]
        CoolLightYellowMini,
    }
}
