﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStudio.Wpf.Mind.Models
{
    public class TreeNode
    {
        public string Text
        {
            get; set;
        }

        public List<TreeNode> Children
        {
            get; set;
        }
    }
}
