﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.Script.Core.Models;
using AIStudio.Wpf.Script.Core.ViewModels;
using System.Collections.Generic;

namespace AIStudio.Wpf.Script.ViewModels
{
    public class ColorBoxIfScriptViewModel : RoslynScriptViewModel
    {
        public ColorBoxIfScriptViewModel()
        {
        }

        public ColorBoxIfScriptViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public ColorBoxIfScriptViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public ColorBoxIfScriptViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
        }

        protected override void InitNew()
        {
            base.InitNew();

            ItemWidth = 80;
            ItemHeight = 80;
            Text = "均衡分配器";
            Code = @"using System;
using System.Collections.Generic;
using AIStudio.Wpf.Script.Core.Models;
using System.Windows.Media;
using System.Linq;

namespace AIStudio.Wpf.CSharpScript
{
    public class ColorBoxIfScript
    {   
        public int Count1 {private get;set;} 
        public int Count2 {private get;set;} 
        public ColorBoxModel Input {private get; set;}
        public ColorBoxModel Output1 {get;private set;}
        public ColorBoxModel Output2 {get;private set;}

        public void Execute()
        {
        	if (Input != null)
            {
                Console.WriteLine($""等待均衡负载{Input.Text}号Box"");
                if (Count1 <= Count2)
                {
                    if (Output1 == null)
                    {
                        Output1 = Input;
                        Input = null;
                        Console.WriteLine($""{Output1.Text}号Box均衡负载完毕,送往1号出口"");
                    }
                }
                else
                {
                    if (Output2 == null)
                    {
                        Output2 = Input;
                        Input = null;
                        Console.WriteLine($""{Output2.Text}号Box均衡负载完毕,送往2号出口"");
                    }
                }
            }
        }
    } 
}";
        }

        private List<ColorBoxModel> _itemsSource;
        public List<ColorBoxModel> ItemsSource
        {
            get
            {
                return _itemsSource;
            }
            set
            {
                SetProperty(ref _itemsSource, value);
            }
        }

      
    }

  
}
